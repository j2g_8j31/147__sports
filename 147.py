#!/usr/bin/python

score = int(raw_input('Please enter score: '))

# list of valid base points
scores = [8, 7, 6, 3]

valid = False

for i in scores:

    # exit if evenly divisable by current score (i)
    if score % i == 0:
        valid = True
        break

    if i < score:
        remainder = score % i

        for j in scores:
            if remainder % j == 0:
                valid = True
                break

if not valid:
    print " -- invalid --"
else:
    print " -- valid --"
